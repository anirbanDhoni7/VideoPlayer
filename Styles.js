import { Dimensions } from "react-native";

export default {
    videoStyle: {
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: '100%'
    },
    modalOuterStyle: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: 'rgba(37, 8, 10, 0.50)',
		alignItems: 'center',
	},
	modalInnerStyle: {
		backgroundColor: '#fff',
		width: 250,
		borderRadius: 4,
		padding: 15,
    },
    modalCloseStyle: {
		alignItems: 'center',
		justifyContent: 'center',
		width: 35,
		height: 35,
		backgroundColor: '#fff',
		borderRadius: 35 / 2,
		elevation: 5,
		marginTop: 30,
    },
    qualityHeaderStyle: {
        borderBottomWidth: 1,
        borderBottomColor: '#cdcdcd',
        alignItems: 'center'
    },
    qualityHeaderTextStyle: {
        fontWeight: 'bold',
        fontSize: 18,
        paddingBottom: 10
    },
    qualityItemStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    volumeIconStyle: {
        position: 'absolute',
        right: 25,
        bottom: 100
    },
    qualityIconStyle: {
        position: 'absolute',
        left: 25,
        bottom: 100
    },
}