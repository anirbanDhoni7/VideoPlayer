import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StatusBar,
  Platform,
  TouchableOpacity,
  Modal,
  FlatList,
  ActivityIndicator
} from 'react-native';
import Video from 'react-native-video';
import styles from './Styles';
import Slider from '@react-native-community/slider';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends React.Component {

  constructor (props) {
    super(props);

    this.state = { 
      videoQualityArray: [180, 270, 406, 720, 1080],
      selectedQuality: 406,
      volume: 1.0,
      isModalVisible: false,
      showSlider: true,
      isMuted: false
    }
  }

  _selectQuality = item => {

    this.setState({ selectedQuality: item }, () => this.setState({isModalVisible: false}))
  }

  _muteUnmute = () => {

    if(this.state.volume==1.0) {
      this.setState({ 
        volume: 0.0, 
        isMuted: !this.state.isMuted 
      })
    } else {
      this.setState({ 
        volume: this.state.volume!=0.0 ? this.state.volume : 1.0, 
        isMuted: !this.state.isMuted 
      });
    }    
  }

  _changeVolume = value => {

    if(value != 0.0) {
      this.setState({ volume: value, isMuted: false });
    } else {
      this.setState({ volume: value, isMuted: true })
    }
  }

  render() {
    return (
      <SafeAreaView>
        <StatusBar backgroundColor="#444"/>
        <Modal
        visible={this.state.isModalVisible}
        transparent
        >
          <View style={styles.modalOuterStyle}>
            <View style={styles.modalInnerStyle}>
              <View style={styles.qualityHeaderStyle}>
                <Text style={styles.qualityHeaderTextStyle}>Select Quality</Text>
              </View>
              <View style={{paddingVertical: 10}}>
                <FlatList
                data={this.state.videoQualityArray}
                keyExtractor={({item, index}) => index}
                ItemSeparatorComponent={() => {return <View style={{height: 10}}/>}}
                renderItem={({item, index}) => (
                  <TouchableOpacity 
                  key={index}
                  style={styles.qualityItemStyle}
                  activeOpacity={0.3}
                  onPress={() => this._selectQuality(item)}
                  >
                    {
                      item!=this.state.selectedQuality ?
                      <Text>{item.toString() + "p"}</Text> :
                      <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                      }}>
                        <Text>{item.toString() + "p"}</Text>
                        <Icon
                        name={Platform.OS==='android' ? 'md-checkmark' : 'ios-checkmark'}
                        size={25}
                        color="#1b1b1b"
                        />
                      </View>
                    }
                  </TouchableOpacity>
                )}
                />
              </View>
            </View>
            
            <TouchableOpacity 
            activeOpacity={0.3}
            onPress={() => this.setState({isModalVisible: false})}
            style={styles.modalCloseStyle}>
              <Icon
                name={Platform.OS==='android' ? 'md-close' : 'ios-close'}
                size={25}
                color="#1b1b1b"            
              />
            </TouchableOpacity>
          </View>
          
        </Modal>
        <Video 
          onLoadStart={() => {return (
            <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <ActivityIndicator size="large" color="#1b1b1b"/>
            </View>
          )}}
          onLoad={() => console.log("loaded")}
          source={{uri: "https://content.jwplatform.com/manifests/yp34SRmf.m3u8"}}   
          ref={(ref) => {
            this.player = ref
          }}                                      
          onError={() => console.log("error")}               
          style={styles.videoStyle} 
          resizeMode="contain"
          volume={this.state.volume}
          selectedVideoTrack={{
            type: "resolution", 
            value: this.state.selectedQuality
          }}
          muted={this.state.isMuted}
          controls
        />

        <TouchableOpacity
        activeOpacity={0.5}
        style={styles.qualityIconStyle}
        onPress={() => this.setState({ isModalVisible: true })}
        >
          <Icon
            name={Platform.OS==='android' ? 'md-settings' : 'ios-settings'}
            size={30}
            color="#1b1b1b"            
          />
        </TouchableOpacity>       
        
        <View style={{
          position: 'absolute',
          bottom: 100,
          right: 25
        }}>
          <View style={{
            borderWidth: 1,
            borderRadius: 10,
            paddingHorizontal: 5,
            borderColor: '#ececec',
            backgroundColor: '#ececec',
            flexDirection: 'row',
            alignItems: 'center'
          }}>
            <TouchableOpacity
            activeOpacity={0.3}
            onPress={() => this._muteUnmute()}
            >
            {
              this.state.isMuted ?
              <Icon
                name={Platform.OS==='android' ? 'md-volume-high' : 'ios-volume-high'}
                size={30}
                color="#1b1b1b"            
              /> :
              <Icon
                name={Platform.OS==='android' ? 'md-volume-mute' : 'ios-volume-mute'}
                size={30}
                color="#1b1b1b"            
              />
              }
            </TouchableOpacity>
            <Slider
              style={{width: 200, height: 40}}
              minimumValue={0.0}
              maximumValue={1.0}
              onValueChange={value => this._changeVolume(value)}
              value={this.state.volume}
              minimumTrackTintColor="#1b1b1b"
              thumbTintColor="#1b1b1b"
              maximumTrackTintColor="#008000"
              // style={{backgroundColor: '#fff'}}
            />
          </View>
        </View>
        
      </SafeAreaView>
    )
  }
}
